#include "car.h"
#include "car_status.h"
#include "io.h"
#include "message.h"
#include "crypt.h"
#include "car_reports.h"
#include "event.h"
#include "event_storage.h"
#include "engine.h"
#include "engine_start_status.h"
#include "utils.h"
#include "defer.h"
#include "slots.h"
#include "phone.h"
#include "setup.h"
#include "sim800.h"

//#include "DS3232RTC.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define DEFAULT_ENGINE_AUTO_STOP_TIMEOUT_MINUTES 15
#define MAX_COUNTER 100

CarStatus carStatus = {};
Message msg = {};

void setAccAsNeededAndReport() {
	bool currentlyAccOn = isAccOn();
	if (carStatus.accStatus && !currentlyAccOn) {
		accOn();
		queueSendStatus();
	} else if (!carStatus.accStatus && currentlyAccOn) {
		accOff();
		queueSendStatus();
	}
}

void startEngineBySchedule() {
	startEngine(carStatus.settings.autostartDurationMinutes);
}

void initializeAutostartIfEnabled(const Settings *newSettings) {
	if (newSettings->autostartEnabled && !isGeneratorOn()) {
		// The autostart should be scheduled.
		uint32_t autostartAfterSeconds = newSettings->autostartIntervalMinutes * 60;

		// The time interval is counted not from current moment, but from last engine stop moment
		int16_t elapsedSeconds = secondsSinceEpoch() - carStatus.lastEngineStopMoment;
		autostartAfterSeconds -= elapsedSeconds;
		if (autostartAfterSeconds < 60) {
			// play it safe: give the user some time to cancel, never start immediately after changing settings
			autostartAfterSeconds = 60;
		}
		// (Re-)schedule
		defer(SL_ENGINE_SCHEDULED_START, startEngineBySchedule, autostartAfterSeconds * 1000UL);
	} else {
		cancelDefer(SL_ENGINE_SCHEDULED_START);
	}
	queueSendStatus();
}

void performAction(uint8_t action) {
	bool status;
	switch (action) {
		case ACTION_START:
			startEngine(DEFAULT_ENGINE_AUTO_STOP_TIMEOUT_MINUTES);
			break;
		case ACTION_STOP:
			turnIgnitionOff();
			queueSendStatus();
			storeEvent(EVENT_ENGINE_STOP, EV_BY_USER, true);
			break;
		case ACTION_STATUS:
			queueSendStatus();
			break;
		case ACTION_LOCK:
			status = lockDoors();
			storeEvent(EVENT_DOOR_LOCK, EV_BY_USER, status);
			break;
		case ACTION_UNLOCK:
			status = unlockDoors();
			storeEvent(EVENT_DOOR_UNLOCK, EV_BY_USER, status);
			break;
		case ACTION_ACC_ON:
			carStatus.accStatus = true;
			break;
		case ACTION_ACC_OFF:
			carStatus.accStatus = false;
			break;
	}
}

bool isNewerSettingsVersion(uint16_t proposedVersion, uint16_t currentVersion) {
	return proposedVersion != currentVersion && proposedVersion - currentVersion < 32000;
}

void applySettings(const Settings *newSettings) {
	if (isNewerSettingsVersion(newSettings->version, carStatus.settings.version)) {
		initializeAutostartIfEnabled(newSettings);
		carStatus.settings = *newSettings;
		char buf[20] = {0};
		sprintf(buf, "set ver=%u", carStatus.settings.version);
		sendText(buf);
	}
}

void processCarMsg(const CarMessage *carMsg) {
	switch (carMsg->type) {
		case MSG_REQUEST_ACTION:
			performAction(carMsg->msgRequestAction.action);
			break;
		case MSG_REQUEST_EVENTS:
			carStatus.lastEventSeq = carMsg->msgRequestEvents.fromSeq-1;
			break;
		case MSG_APPLY_SETTINGS:
			applySettings(&carMsg->msgApplySettings.settings);
			break;
	}
}
/*
void processBt(Message *msg) {
	if (btReadMessage(msg)) {
		CarMessage carMsg;
		if (decrypt(msg, &carMsg)) {
			processCarMsg(&carMsg);
		} else {
			sendText("BadCrypt");
		}
		clearMessage(msg);
	}
}
*/
void initDefaultSettings(Settings *settings) {
	settings->version = 0;
	settings->driftCompensation = 0;
	settings->engineTimeoutMillis = 10000;
	settings->autostartEnabled = false;
	settings->autostartDurationMinutes = 10;
	settings->autostartIntervalMinutes = 300;
}

void carSetup() {
	saveResetReason();
	ioSetup();
	
	gsmSetup();
	//return;
	//while(1);

	reportResetReason();

	ignitionOff();
	starterOff();
	statusLedOff();

	carStatus.lastStatus = 0;
	carStatus.accStatus = false;
	carStatus.parked = isParked();
	carStatus.generator = isGeneratorOn();
	carStatus.lastEventSeq = getLastSeq();
	carStatus.lastEngineStopMoment = secondsSinceEpoch();

//	clearMessage(&msg);
	
	statusLedOn();
	generateRandomSeed();
	statusLedOff();

	//uint32_t t = RTC.get();
	//RTC.set(t+662311759L);

	initDefaultSettings(&carStatus.settings);
	if (!isBadReset()) {
		initializeAutostartIfEnabled(&carStatus.settings);
	}
	
	sendStatus();
}

void carLoop() {
	if (gsmAvailable()) {
		gsmProcess(/*&msg*/);
	}
	carBackgroundActions();
}

void handleParkedChanged(bool parked) {
	// never autorun when not parked
	if (!parked) {
		turnIgnitionOff();
		sendText("unparked");
	} else {
		sendText("parked");
	}
}

bool isEngineAutoRunning() {
	return isActiveDefer(SL_ENGINE_AUTO_STOP);
}

void handleGeneratorChanged(bool generator) {
	if (generator) {
		if (!isEngineAutoRunning()) {
			storeEvent(EVENT_ENGINE_START, EV_BY_ENVIRONMENT, true);
		}
	} else {
		carStatus.lastEngineStopMoment = secondsSinceEpoch();
		// if the generator turned off, but we are autorunning now - this is not normal, shut down
		if (isEngineAutoRunning()) {
			turnIgnitionOff();
			storeEvent(EVENT_ENGINE_STOP, EV_BY_ERROR, true);
		} else if (carStatus.lastStatus != STARTED) {
			storeEvent(EVENT_ENGINE_STOP, EV_BY_ENVIRONMENT, true);
		}
	}
	initializeAutostartIfEnabled(&carStatus.settings);
}

void carBackgroundActions() {
	runDeferred();

	//  if we are not starting the engine now
	if (!isActiveDefer(SL_ENGINE_START)) {
		bool parked = isParked();
		bool generator = isGeneratorOn();
	
		if (parked != carStatus.parked) {
			carStatus.parkedChangeCounter++;
		}
		if (carStatus.parkedChangeCounter >= MAX_COUNTER) {
			carStatus.parkedChangeCounter = 0;
			handleParkedChanged(parked);
			carStatus.parked = parked;
			queueSendStatus();
		}
		if (generator != carStatus.generator) {
			carStatus.generatorChangeCounter++;
		}
		if (carStatus.generatorChangeCounter >= MAX_COUNTER) {
			carStatus.generatorChangeCounter = 0;
			handleGeneratorChanged(generator);
			carStatus.generator = generator;
			queueSendStatus();
		}

		setAccAsNeededAndReport();

		unsigned int newLastSeq = getLastSeq();
		sendEvents(carStatus.lastEventSeq, newLastSeq);
		carStatus.lastEventSeq = newLastSeq;
	}
}
