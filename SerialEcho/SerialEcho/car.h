
#if(!defined CAR_H)
#define CAR_H

#include "settings.h"

bool isEngineAutoRunning();

void performAction(uint8_t action);

void carSetup();

void carLoop();

void carBackgroundActions();

//  HACK: used in unit tests
void initDefaultSettings(Settings *settings);
void applySettings(const Settings *newSettings);
//  END HACK

#endif