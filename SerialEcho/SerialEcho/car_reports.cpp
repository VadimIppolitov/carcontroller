#include "car_reports.h"
#include "car_status.h"
#include "message.h"
#include "crypt.h"
#include "event_storage.h"
#include "utils.h"

#include <string.h>

void fillRemaningNonce(Message *msg) {
	for (int i = 0; i < sizeof(msg->remainingNonce) / sizeof(int); ++i) {
		uint32_t rnd = randomUInt();
		memcpy(&msg->remainingNonce[i*sizeof(int)], &rnd, sizeof(int));
	}
}

void sendCarMessage(const CarMessage& carMsg) {
	Message msg;
	msg.microsSinceStart = currentMicros();
	msg.secondsSinceEpoch = secondsSinceEpoch();
	msg.source = SOURCE_CAR_CONTROLLER;
	fillRemaningNonce(&msg);
	encrypt(carMsg, &msg);
	//btSendMessage(&msg);//TODO
}

void sendStatus(const CarStatus& carStatus, bool ignitionStatus, unsigned int minutesUntilScheduledStart) {
	CarMessage carMsg = {
		.type = MSG_STATUS
	};
	carMsg.msgStatus.gearStatus = carStatus.parked ? 'P' : '?';
	carMsg.msgStatus.generatorStatus = carStatus.generator;
	carMsg.msgStatus.temperature = temperatureCelsius();
	carMsg.msgStatus.ignitionStatus = ignitionStatus;
	carMsg.msgStatus.lastStatus = carStatus.lastStatus;
	carMsg.msgStatus.lastStarterTime = carStatus.lastStarterTime;
	carMsg.msgStatus.accStatus = carStatus.accStatus;
	carMsg.msgStatus.lastEventSeq = carStatus.lastEventSeq;
	carMsg.msgStatus.minutesUntilScheduledStart = minutesUntilScheduledStart;

	//blink(500);
	sendCarMessage(carMsg);
}

void sendSettings(const Settings& settings) {
	CarMessage carMsg = {
		.type = MSG_REPORT_SETTINGS
	};
	carMsg.msgReportSettings.settings = settings;

	//blink(1000);
	sendCarMessage(carMsg);
}

bool fillEventList(unsigned int firstInMessage, unsigned char cntInMessage, CarMsgEventList* eventList) {
	eventList->firstSeq = firstInMessage;
	eventList->eventCount = cntInMessage;
	for (int e = 0; e < cntInMessage; e++) {
		if (!fetchEvent(firstInMessage + e, &eventList->events[e])) {
			sendText("Failed!");
			return false;
		}
	}
	return true;
}

void sendEvents(uint16_t firstEvent, uint16_t lastEvent) {
	uint16_t cntNewEvents = lastEvent - firstEvent;
	uint16_t i = 0;
	while (i < cntNewEvents) {
		CarMessage carMsg = {
			.type = MSG_EVENTS_LIST
		};
		uint8_t cntInMessage = min(cntNewEvents - i, MAX_EVENTS_IN_MESSAGE);
		if (fillEventList(firstEvent + i + 1, cntInMessage, &carMsg.msgEventList)) {
			//blink(2000);
			sendCarMessage(carMsg);
		}
		i += cntInMessage;
	}
}

void sendText(const char *text) {
	CarMessage carMsg = {
		.type = MSG_TEXT
	};
	memset(carMsg.msgText.text, 0, sizeof(carMsg.msgText.text));
	strncpy(carMsg.msgText.text, text, sizeof(carMsg.msgText.text));
	
	//blink(4000);
	sendCarMessage(carMsg);
}

