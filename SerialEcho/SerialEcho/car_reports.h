#if(!defined CAR_REPORTS_H)
#define CAR_REPORTS_H

#include "car_status.h"
#include "message.h"

void sendCarMessage(const CarMessage& carMsg);

void sendStatus(const CarStatus& carStatus, bool ignitionStatus, unsigned int minutesUntilScheduledStart);

void sendSettings(const Settings& settings);

void sendEvents(unsigned int firstEvent, unsigned int lastEvent);

void sendText(const char *text);

#endif