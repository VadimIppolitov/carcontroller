#include "car_status.h"
#include "slots.h"
#include "defer.h"
#include "event.h"
#include "car_reports.h"
#include "engine_start_status.h"

//  Timeouts in ms
#define WAIT_STATUS_SEND 30*1000

extern CarStatus carStatus;

void sendStatus() {
	bool ignitionStatus = isActiveDefer(SL_ENGINE_AUTO_STOP); // replace with isEngineAutoRunning?
	unsigned int minutesUntilScheduledStart = timeRemaining(SL_ENGINE_SCHEDULED_START) / 60 / 1000L;
	sendStatus(carStatus, ignitionStatus, minutesUntilScheduledStart);
	sendSettings(carStatus.settings);
	defer(SL_SEND_STATUS, sendStatus, WAIT_STATUS_SEND);
}

void queueSendStatus() {
	defer(SL_SEND_STATUS, sendStatus, 0);
}

void setAndReportStartStatus(uint8_t status) {
	carStatus.lastStatus = status;
	queueSendStatus();
	if (status != STARTING && status != VERIFYING) {
		uint8_t by = carStatus.settings.autostartEnabled ? EV_BY_SCHEDULE : EV_BY_USER;
		storeEvent(EVENT_ENGINE_START, by, status == STARTED || status == HOLD);
	}
}
