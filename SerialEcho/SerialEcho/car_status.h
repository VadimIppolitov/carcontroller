#if(!defined CAR_STATUS_H)
#define CAR_STATUS_H

#include <stdint.h>
#include "settings.h"

typedef struct CarStatus {
	uint8_t lastStatus;
	uint16_t lastStarterTime;
	bool accStatus;
	bool parked;
	uint16_t parkedChangeCounter;
	bool generator;
	uint16_t generatorChangeCounter;
	uint16_t lastEventSeq;
	Settings settings;
	uint32_t lastEngineStopMoment;
} CarStatus;

void sendStatus();

void queueSendStatus();

void setAndReportStartStatus(unsigned char status);

#endif