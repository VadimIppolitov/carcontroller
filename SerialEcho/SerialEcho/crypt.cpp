
#include "crypt.h"
#include "avrnacl.h"
#include <string.h>

#include "key.h"

const unsigned char zeros[128] = {0};

void packNonce(unsigned char *nonce, const Message *msg) {
	int offset = 0;
	memcpy(&nonce[offset], &msg->secondsSinceEpoch, sizeof(msg->secondsSinceEpoch));
	offset += sizeof(msg->secondsSinceEpoch);
	memcpy(&nonce[offset], &msg->microsSinceStart, sizeof(msg->microsSinceStart));
	offset += sizeof(msg->microsSinceStart);
	memcpy(&nonce[offset], &msg->source, sizeof(msg->source));
	offset += sizeof(msg->source);
	memcpy(&nonce[offset], &msg->remainingNonce[0], sizeof(msg->remainingNonce));
}

void unpackNonce(Message *msg, const unsigned char *nonce) {
	int offset = 0;
	memcpy(&msg->secondsSinceEpoch, &nonce[offset], sizeof(msg->secondsSinceEpoch));
	offset += sizeof(msg->secondsSinceEpoch);
	memcpy(&msg->microsSinceStart, &nonce[offset], sizeof(msg->microsSinceStart));
	offset += sizeof(msg->microsSinceStart);
	memcpy(&msg->source, &nonce[offset], sizeof(msg->source));
	offset += sizeof(msg->source);
	memcpy(&msg->remainingNonce[0], &nonce[offset], sizeof(msg->remainingNonce));
}

void encrypt(const CarMessage& carMsg, Message *msg) {
	//  Prepare plaintext with zeros in front
	uint8_t plaintext[crypto_secretbox_ZEROBYTES + MESSAGE_DATA_MAX_LEN];
	memset(&plaintext[0], 0, crypto_secretbox_ZEROBYTES);
	memcpy(&plaintext[crypto_secretbox_ZEROBYTES], &carMsg, sizeof(CarMessage));
	
	//  Prepare nonce
	uint8_t nonce[crypto_secretbox_NONCEBYTES];
	packNonce(nonce, msg);
	memcpy(&msg->data[0], nonce, crypto_secretbox_NONCEBYTES);
	
	uint8_t cyphertext[crypto_secretbox_BOXZEROBYTES + MESSAGE_DATA_MAX_LEN];
	
	//  Encrypt plaintext -> ciphertext
	msg->size = crypto_secretbox_ZEROBYTES + sizeof(CarMessage);
	crypto_secretbox(cyphertext, plaintext, msg->size, nonce, key);
	msg->size -= crypto_secretbox_BOXZEROBYTES;
	memcpy(&msg->data[crypto_secretbox_NONCEBYTES], &cyphertext[crypto_secretbox_BOXZEROBYTES], msg->size);

	msg->pos = 0;
	msg->size += crypto_secretbox_NONCEBYTES;
}

bool decrypt(Message *msg, CarMessage *carMsg) {
	//  Prepare nonce
	uint8_t nonce[crypto_secretbox_NONCEBYTES];
	memcpy(&nonce[0], &msg->data[msg->pos], crypto_secretbox_NONCEBYTES);
	msg->pos += crypto_secretbox_NONCEBYTES;
	msg->size -= crypto_secretbox_NONCEBYTES;
	unpackNonce(msg, nonce);
	
	//  Prepare cyphertext with zeros in front
	uint8_t cyphertext[crypto_secretbox_BOXZEROBYTES + MESSAGE_DATA_MAX_LEN];
	memset(&cyphertext[0], 0, crypto_secretbox_BOXZEROBYTES);
	memcpy(&cyphertext[crypto_secretbox_BOXZEROBYTES], &msg->data[msg->pos], msg->size);
	
	//  Decrypt cyphertext -> msg->data
	msg->size += crypto_secretbox_BOXZEROBYTES;
	if (crypto_secretbox_open(msg->data, cyphertext, msg->size, nonce, key) == 0) {
		//  No need to see the zeros in front
		msg->pos = crypto_secretbox_ZEROBYTES;
		msg->size -= crypto_secretbox_ZEROBYTES;
		
		//  TODO: verify time?
		if (true) {
			memcpy(carMsg, &msg->data[msg->pos], sizeof(CarMessage));
			return true;
		}
	}
	return false;
}
