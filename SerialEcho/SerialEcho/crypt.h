#if(!defined CRYPT_H)
#define CRYPT_H

#include "message.h"

void encrypt(const CarMessage& carMsg, Message *msg);
bool decrypt(Message *msg, CarMessage *carMsg);

#endif