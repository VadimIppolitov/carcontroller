
#ifndef DEBUG_H_
#define DEBUG_H_

//#undef DEBUG

#if (defined DEBUG)
#include "HardwareSerial.h"

extern HardwareSerial& debugStream;
#endif

#endif