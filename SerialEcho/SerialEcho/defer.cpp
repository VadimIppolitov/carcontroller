#include <stddef.h>
#include "defer.h"
#include "utils.h"
#include "slots.h"

Deferred callbacks[SLOT_COUNT] = {0};
uint32_t triggerTimes[SLOT_COUNT] = {0};

void defer(Slot slot, Deferred callback, uint32_t ms) {
	if (slot < SLOT_COUNT) {
		triggerTimes[slot] = currentMs() + ms;
		callbacks[slot] = callback;
	}
}

void cancelDefer(Slot slot) {
	if (slot < SLOT_COUNT) {
		callbacks[slot] = NULL;
	}
}

bool isActiveDefer(Slot slot) {
	return slot < SLOT_COUNT && callbacks[slot] != NULL;
}

int32_t timeRemaining(Slot slot) {
	if (isActiveDefer(slot)) {
		return triggerTimes[slot] - currentMs(); // implicit cast, OK
	}
	return 0;
}

void runDeferred() {
	uint32_t ms = currentMs();
	for (int slot = 0; slot < SLOT_COUNT; ++slot) {
		if (callbacks[slot] != NULL && (int32_t)ms - (int32_t)triggerTimes[slot] >= 0) {
			Deferred callback = callbacks[slot];
			callbacks[slot] = NULL;  //  Clear first, to allow to reschedule from inside the callback
			callback();
		}
	}
}