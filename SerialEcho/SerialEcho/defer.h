
#if (!defined DEFER_H)
#define DEFER_H

#include <stdint.h>

typedef void (*Deferred) ();
typedef unsigned char Slot;

void defer(Slot slot, Deferred callback, uint32_t ms);
void cancelDefer(Slot slot);
bool isActiveDefer(Slot slot);
int32_t timeRemaining(Slot slot);

void runDeferred();

#endif