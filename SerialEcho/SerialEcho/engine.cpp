
#include "engine.h"
#include "engine_start_status.h"
#include "car_reports.h"
#include "io.h"
#include "event.h"
#include "defer.h"
#include "slots.h"
#include "utils.h"

//  Timeouts in ms
#define WAIT_WHEN_STARTING 50
#define WAIT_AFTER_START 250
#define WAIT_AFTER_IGN 3000
#define WAIT_BEFORE_VERIFICATION 1000

#define COUNT_GEN_THRESH (WAIT_AFTER_START / WAIT_WHEN_STARTING)

extern CarStatus carStatus;

void turnIgnitionOff() {
	ignitionOff();
	statusLedOff();
	carStatus.lastStatus = 0;

	cancelDefer(SL_ENGINE_START);
	cancelDefer(SL_ENGINE_AUTO_STOP);
}

void autoStop() {
	turnIgnitionOff();
	queueSendStatus();
	storeEvent(EVENT_ENGINE_STOP, EV_BY_SCHEDULE, true);
}

void turnIgnitionOn(uint16_t runDurationMinutes) {
	ignitionOn();
	statusLedOn();
	defer(SL_ENGINE_AUTO_STOP, &autoStop, runDurationMinutes * 60 * 1000L);
}

void startEngine(uint16_t runDurationMinutes) {
	if (isActiveDefer(SL_ENGINE_START)) {
		//  already starting by command, maybe a duplicate packet/command - report ok
		setAndReportStartStatus(STARTING);
	} else if (isGeneratorOn()) {
		//  engine already running
		if (isParked()) {
			turnIgnitionOn(runDurationMinutes);
			setAndReportStartStatus(HOLD);
		} else {
			setAndReportStartStatus(ALREADY_STARTED);
		}
	} else {
		//  Otherwise the engine is not running
		//  Entering the normal startup sequence
		accOff();
		turnIgnitionOn(runDurationMinutes);
		//  Wait for self-checks to pass
		defer(SL_ENGINE_START, &startEngineAfterIgn, WAIT_AFTER_IGN);
		setAndReportStartStatus(STARTING);
	}
}

void startEngineAfterIgn() {
	if (isGeneratorOn()) {
		//  Suddenly! OK, back off
		turnIgnitionOff();
		setAndReportStartStatus(ALREADY_STARTED);
		return;
	}

	if (!isParked()) {
		turnIgnitionOff();
		setAndReportStartStatus(NOT_PARKED);
		return;
	}

	starterOn();  //  !!! START !!!
	uint32_t starterStartTime = currentMs();
	uint16_t countGen = 0;

	// TODO: get rid of delays
	for (uint16_t t = 0; t < carStatus.settings.engineTimeoutMillis && countGen < COUNT_GEN_THRESH; t += WAIT_WHEN_STARTING) {
		delayMs(WAIT_WHEN_STARTING);
		if (isGeneratorOn()) {
			countGen++;
		} else {
			countGen = 0;
		}
	}

	if (!isGeneratorOn()) {
		starterOff();
		turnIgnitionOff();
		setAndReportStartStatus(ENGINE_TIMEOUT);
		return;
	}

	carStatus.lastStarterTime = currentMs() - starterStartTime;
	starterOff();
	//  Wait a bit after turning off starter and then validate the generator status
	defer(SL_ENGINE_START, &verifyEngineStillRunning, WAIT_BEFORE_VERIFICATION);
	setAndReportStartStatus(VERIFYING);
}

void verifyEngineStillRunning() {
	if (isGeneratorOn()) {
		setAndReportStartStatus(STARTED);
	} else {
		turnIgnitionOff();
		setAndReportStartStatus(ENGINE_ERROR);
	}
}