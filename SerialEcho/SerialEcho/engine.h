
#if (!defined ENGINE_H)
#define ENGINE_H

#include <stdint.h>

void turnIgnitionOff();

void startEngine(uint16_t runDurationMinutes);

void startEngineAfterIgn();

void verifyEngineStillRunning();

#endif