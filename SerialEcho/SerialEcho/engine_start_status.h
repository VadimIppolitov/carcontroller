
#ifndef ENGINE_START_STATUS_H_
#define ENGINE_START_STATUS_H_

//  Engine start status codes
#define STARTED 1
#define HOLD 2
#define ENGINE_ERROR 3
#define NOT_PARKED 4
#define ALREADY_STARTED 5
#define ENGINE_TIMEOUT 6
#define STARTING 7
#define VERIFYING 8

#endif /* ENGINE_START_STATUS_H_ */