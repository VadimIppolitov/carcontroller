#if (!defined EVENT_H)
#define EVENT_H

#define EVENT_ENGINE_START 31
#define EVENT_ENGINE_STOP 32
#define EVENT_DOOR_LOCK 34
#define EVENT_DOOR_UNLOCK 35
#define EVENT_ACC_ON 36
#define EVENT_ACC_OFF 37

#define EV_BY_USER 41
#define EV_BY_SCHEDULE 42
#define EV_BY_ENVIRONMENT 43
#define EV_BY_ERROR 44

#endif