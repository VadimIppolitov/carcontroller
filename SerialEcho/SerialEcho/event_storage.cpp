#include <string.h>
#include "event_storage.h"
#include "utils.h"

//  Circular buffer
StoredEvent events[MAX_STORED_EVENTS] = {0};
uint16_t eventSeq = 0;

uint8_t eventPos = 0;

uint16_t storeEvent(StoredEvent *event) {
	event->secondsSinceEpoch = secondsSinceEpoch();
	memcpy(&events[eventPos], event, sizeof(StoredEvent));
	eventPos = (eventPos + 1) % MAX_STORED_EVENTS;
	return eventSeq++;
}

uint16_t storeEvent(uint8_t event, uint8_t by, bool success) {
	StoredEvent storedEvent;
	storedEvent.event = event;
	storedEvent.initiatedBy = by;
	storedEvent.success = success;
	return storeEvent(&storedEvent);
}

uint16_t getLastSeq() {
	return eventSeq-1;
}

bool fetchEvent(uint16_t seq, StoredEvent *event) {
	if (seq >= eventSeq) {
		return false;
	}
	if (eventSeq - seq > MAX_STORED_EVENTS) {
		return false;
	}
	uint8_t pos = (eventPos + seq - eventSeq + MAX_STORED_EVENTS) % MAX_STORED_EVENTS;
	memcpy(event, &events[pos], sizeof(StoredEvent));
	return true;
}

void clearAllEvents() {
	memset(&events[0], 0, sizeof(events));
	eventSeq = 0;
	eventPos = 0;
}