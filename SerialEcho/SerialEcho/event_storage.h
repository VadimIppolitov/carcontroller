#if (!defined EVENT_STORAGE_H)
#define EVENT_STORAGE_H

#include <stdint.h>

#define MAX_STORED_EVENTS 42

typedef struct __attribute__((__packed__)) StoredEvent {
	uint32_t secondsSinceEpoch;
	uint8_t event;
	uint8_t initiatedBy;
	bool success;
} StoredEvent;

uint16_t storeEvent(StoredEvent *event);
uint16_t storeEvent(uint8_t event, uint8_t by, bool success);

uint16_t getLastSeq();
bool fetchEvent(uint16_t seq, StoredEvent *event);

void clearAllEvents();  //  for testing

#endif