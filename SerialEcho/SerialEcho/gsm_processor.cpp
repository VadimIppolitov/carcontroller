
#include <avr/pgmspace.h>

#include "sim800.h"
#include "message.h"
#include "car.h"
#include "debug.h"

const char VADIPP_PHONE[] PROGMEM = "79607984663";
const char JOLY_PHONE[] PROGMEM = "79612232616";

void processGsmRing(const char* phoneNumber) {
	#if (defined DEBUG)
	debugStream.print(F("GSM PROC: phone number = "));
	debugStream.println(phoneNumber);
	#endif
	if (!isEngineAutoRunning()) {
		if (strncmp_P(phoneNumber, VADIPP_PHONE, sizeof(VADIPP_PHONE)) == 0 || strncmp_P(phoneNumber, JOLY_PHONE, sizeof(JOLY_PHONE)) == 0) {
			performAction(ACTION_START);
			#if (defined DEBUG)
			debugStream.println(F("GSM PROC: engine started"));
			#endif
		} else {
			#if (defined DEBUG)
			debugStream.print(F("GSM PROC ERROR: phone number unknown: "));
			debugStream.println(phoneNumber);
			#endif
		}
	} else {
		#if (defined DEBUG)
		debugStream.println(F("GSM PROC ERROR: engine autorunning"));
		#endif
	}
	gsmHangupCall();
}

void processGsmSms(const char* phoneNumber, const char* smsText) {
	//TODO
}