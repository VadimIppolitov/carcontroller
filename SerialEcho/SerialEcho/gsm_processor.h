
#ifndef GSMPROCESSOR_H_
#define GSMPROCESSOR_H_

void processGsmRing(const char* phoneNumber);

void processGsmSms(const char* phoneNumber, const char* smsText);

#endif /* GSMPROCESSOR_H_ */