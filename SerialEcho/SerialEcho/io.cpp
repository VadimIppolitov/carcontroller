#include <Arduino.h>
#include "io.h"
#include "defer.h"
#include "slots.h"
#include "pins.h"

//  Timeouts
#define DOOR_LOCK_DELAY 250

//  Operations

void ignitionOff() {
	digitalWrite(IGN, LOW);
}

void ignitionOn() {
	digitalWrite(IGN, HIGH);
}

void starterOff() {
	digitalWrite(STRTR, LOW);
}

void starterOn() {
	digitalWrite(STRTR, HIGH);
}

void statusLedOff() {
	digitalWrite(LED, LOW);
}

void statusLedOn() {
	digitalWrite(LED, HIGH);
}

void accOff() {
	digitalWrite(ACC, LOW);
}

void accOn() {
	digitalWrite(ACC, HIGH);
}

bool isAccOn() {
	return digitalRead(ACC);
}

void finishLockDoors() {
	pinMode(LOCK_DOORS, INPUT);
}

void finishUnlockDoors() {
	pinMode(UNLOCK_DOORS, INPUT);
}

bool operateDoors(uint8_t pin, Deferred handler) {
	if (isActiveDefer(SL_DOOR_CONTROL)) {
		return false;
	}
	pinMode(pin, OUTPUT);
	defer(SL_DOOR_CONTROL, handler, DOOR_LOCK_DELAY);
	return true;
}

bool lockDoors() {
	return operateDoors(LOCK_DOORS, &finishLockDoors);
}

bool unlockDoors() {
	return operateDoors(UNLOCK_DOORS, &finishUnlockDoors);
}

void phonePowerBoardOn() {
	digitalWrite(PHONE_PWR_BOARD, 0);
}

void phonePowerBoardOff() {
	digitalWrite(PHONE_PWR_BOARD, 1);
}

void phonePowerButtonDown() {
	pinMode(PHONE_PWR_BTN, OUTPUT);
}

void phonePowerButtonUp() {
	pinMode(PHONE_PWR_BTN, INPUT);
}

int16_t readFloatingPin() {
	return analogRead(FLOATING_PIN);
}

//  Status checks

bool isParked() {
	return digitalRead(PARK_ON);
}

bool isGeneratorOn() {
	return digitalRead(GEN_ON);
}

//  Setup

void ioSetup() {
	pinMode(IGN, OUTPUT);
	pinMode(STRTR, OUTPUT);
	pinMode(LED, OUTPUT);
	pinMode(ACC, OUTPUT);

	pinMode(PARK_ON, INPUT);
	pinMode(GEN_ON, INPUT);

	pinMode(LOCK_DOORS, INPUT);
	pinMode(UNLOCK_DOORS, INPUT);
	
	//pinMode(PHONE_PWR_BOARD, OUTPUT);
	
	digitalWrite(LOCK_DOORS, LOW);
	digitalWrite(UNLOCK_DOORS, LOW);
	
	//phonePowerBoardOn();
	//phonePowerButtonUp();
	
	//digitalWrite(PHONE_PWR_BTN, LOW);
}
