#if (!defined IO_H)
#define IO_H

#include <stdint.h>

//  Operations

void ignitionOff();
void ignitionOn();

void starterOff();
void starterOn();

void statusLedOff();
void statusLedOn();

void accOff();
void accOn();
bool isAccOn();

bool lockDoors();
bool unlockDoors();

void phonePowerBoardOn();
void phonePowerBoardOff();
void phonePowerButtonDown();
void phonePowerButtonUp();

int16_t readFloatingPin();

//  Status checks

bool isParked();

bool isGeneratorOn();

//  Setup

void ioSetup();

#endif