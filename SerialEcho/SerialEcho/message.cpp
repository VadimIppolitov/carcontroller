
#include "message.h"
#include "io.h"
#include "utils.h"
/*
bool btReadMessage(Message *msg) {
	//  Skipping preamble markers
	static int16_t markersRemaining = PREAMBLE_LENGTH;
	while (markersRemaining > 0 && btAvailable()) {
		int16_t byte = btRead();
		if (byte == 0) {
			markersRemaining--;
		} else {
			markersRemaining = PREAMBLE_LENGTH;
		}
	}
	if (markersRemaining > 0) {
		return false;
	}

	//  Reading size of data packet
	if (msg->size == 0 && btAvailable()) {
		msg->size = btRead();
		if (msg->size > sizeof(msg->data)) {
			msg->size = sizeof(msg->data);
		}
	}
	if (msg->size == 0) {
		return false;
	}

	//  Reading data
	while (msg->limit < msg->size && btAvailable()) {
		msg->data[msg->limit++] = btRead();
	}

	if (msg->limit < msg->size) {
		return false;
	}

	//  All OK
	markersRemaining = PREAMBLE_LENGTH;
	return true;
}

void btSendMessage(const Message *msg) {
	uint8_t preamble[PREAMBLE_LENGTH] = {0};
	btSend(preamble, PREAMBLE_LENGTH);
	btSend(&msg->size, 1);
	btSend(&msg->data[msg->pos], msg->size);
}

void clearMessage(Message *msg) {
	msg->size = 0;
	msg->pos = 0;
	msg->limit = 0;
}
*/