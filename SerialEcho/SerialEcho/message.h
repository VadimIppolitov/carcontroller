
#if(!defined MESSAGE_H)
#define MESSAGE_H

#include <stdint.h>
#include "event_storage.h"
#include "settings.h"

//  Bluetooth communication

#define MESSAGE_DATA_MAX_LEN 64
#define NONCE_LEN 24  //  Must be equal to crypto_secretbox_NONCEBYTES

//  Number of zeros in the preamble sequence
#define PREAMBLE_LENGTH 4

// Bluetooth-link-level message:
// 00 00 00 00 SIZE [DATA]

#define MSG_STATUS 1
#define MSG_REQUEST_ACTION 2
#define MSG_TEXT 3
#define MSG_EVENTS_LIST 6
#define MSG_REQUEST_EVENTS 7
#define MSG_APPLY_SETTINGS 8
#define MSG_REPORT_SETTINGS 9

#define ACTION_START 21
#define ACTION_STOP 22
#define ACTION_STATUS 23
#define ACTION_LOCK 24
#define ACTION_UNLOCK 25
#define ACTION_ACC_ON 26
#define ACTION_ACC_OFF 27

#define SOURCE_UNKNOWN 0
#define SOURCE_PLAINTEXT 1
#define SOURCE_SERVER 11
#define SOURCE_CAR_CONTROLLER 21
#define SOURCE_CAR_PILOT 31
#define SOURCE_REMOTE 41

//  Business-level messages

typedef struct __attribute__((__packed__)) CarMsgStatus {
	bool ignitionStatus;
	bool generatorStatus;
	char gearStatus; // P or ?
	int8_t temperature;
	uint8_t lastStatus;
	uint16_t lastStarterTime;
	bool accStatus;
	uint16_t lastEventSeq;
	uint16_t minutesUntilScheduledStart;
} CarMsgStatus;

typedef struct __attribute__((__packed__)) CarMsgRequestAction {
	uint8_t action;
} CarMsgRequestAction;

typedef struct __attribute__((__packed__)) CarMsgText {
	char text[15];
} CarMsgText;

#define MAX_EVENTS_IN_MESSAGE 1
typedef struct __attribute__((__packed__)) CarMsgEventList {
	uint8_t eventCount;
	uint16_t firstSeq;
	StoredEvent events[MAX_EVENTS_IN_MESSAGE];
} CarMsgEventList;

typedef struct __attribute__((__packed__)) CarMsgRequestEvents {
	uint16_t fromSeq;
} CarMsgRequestEvents;

typedef struct __attribute__((__packed__)) CarMsgApplySettings {
	Settings settings;
} CarMsgApplySettings;

typedef struct __attribute__((__packed__)) CarMsgReportSettings {
	Settings settings;
} CarMsgReportSettings;

typedef struct __attribute__((__packed__)) CarMessage {
	uint8_t type;
	union {
		CarMsgStatus msgStatus;
		CarMsgRequestAction msgRequestAction;
		CarMsgText msgText;
		CarMsgEventList msgEventList;
		CarMsgRequestEvents msgRequestEvents;
		CarMsgApplySettings msgApplySettings;
		CarMsgReportSettings msgReportSettings;
	};
} CarMessage;

//  Transport-level message
typedef struct Message {
	uint8_t size;
	uint8_t pos;
	uint8_t limit;
	
	uint32_t secondsSinceEpoch;
	uint32_t microsSinceStart;
	char source;
	uint8_t remainingNonce[NONCE_LEN-sizeof(secondsSinceEpoch)-sizeof(microsSinceStart)-sizeof(source)];

	uint8_t data[MESSAGE_DATA_MAX_LEN];
} Message;

// Cleanup the object to prepare for reading next message
void clearMessage(Message *msg);

#endif