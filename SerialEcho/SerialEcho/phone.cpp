#include "phone.h"
#include "slots.h"
#include "io.h"
#include "defer.h"
#include "utils.h"

//  Timeouts
#define WAIT_BEFORE_REBOOT 6UL*60*1000
#define PHONE_POWER_DELAY 2000

void phonePowerBackOn() {
	phonePowerBoardOn();
	phonePowerButtonDown();
	defer(SL_PHONE_REBOOT, &phonePowerButtonUp, PHONE_POWER_DELAY);
}

void rebootPhone() {
	phonePowerBoardOff();
	defer(SL_PHONE_REBOOT, &phonePowerBackOn, PHONE_POWER_DELAY);
	schedulePhoneReboot();
}

void schedulePhoneReboot() {
	defer(SL_PHONE_REBOOT_WAIT, rebootPhone, WAIT_BEFORE_REBOOT);
}