#if (!defined PINS_H)
#define PINS_H

//  ---- OUTPUT PINS ---------------

//  Relay control pins
#define IGN 2
#define STRTR 3
#define ACC 5

//  Door control
#define LOCK_DOORS 6
#define UNLOCK_DOORS 9

//  Human interface outputs
#define LED 13

//  Phone controls
#define PHONE_PWR_BOARD A2
#define PHONE_PWR_BTN A3

//  ----- INPUT PINS ---------------

//  Vehicle status inputs
#define PARK_ON 8
#define GEN_ON 7

// Floating pin for random seed generation
#define FLOATING_PIN A7

//  ----- SIM800 ---------------
#define SIM800_RX_PIN 10
#define SIM800_TX_PIN 11
#define SIM800_RST_PIN 4

#endif