
#if(!defined SETTINGS_H)
#define SETTINGS_H

#include <stdint.h>

typedef struct __attribute__((__packed__)) Settings {
	uint16_t version;
	int16_t driftCompensation;
	uint16_t engineTimeoutMillis;
	bool autostartEnabled;
	uint16_t autostartIntervalMinutes;
	uint16_t autostartDurationMinutes;
} Settings;

#endif
