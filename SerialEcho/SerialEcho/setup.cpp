
#include <avr/io.h>
#include <stdio.h>
#include "car_reports.h"

uint8_t resetReason = 0;

void saveResetReason() {
	resetReason = MCUSR;
	MCUSR = 0;
}

void reportResetReason() {
	if (resetReason & (1<<PORF)) {
		sendText("power-on reset");
	} else if (resetReason & (1<<EXTRF)) {
		sendText("external reset");
	} else if (resetReason & (1<<BORF)) {
		sendText("brown-out reset");
	} else if (resetReason & (1<<WDRF)) {
		sendText("watchdog reset");
	} else {
		char buf[10];
		sprintf(buf, "reset %u", resetReason);
		sendText(buf);
	}
}

bool isBadReset() {
	return resetReason == 0;
}