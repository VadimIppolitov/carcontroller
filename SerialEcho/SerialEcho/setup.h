#if (!defined SETUP_H)
#define SETUP_H

void saveResetReason();
void reportResetReason();
bool isBadReset();

#endif