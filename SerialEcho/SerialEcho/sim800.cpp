#include <Arduino.h>
#include <SoftwareSerial.h>

#include "SIM800L.h"
#include "pins.h"
#include "utils.h"
#include "debug.h"
#include "gsm_processor.h"

SIM800L* sim800l;

const char NOTIF_CLIP[] PROGMEM = "+CLIP: ";  // Example: +CLIP: "79607984663",145,"",0,"",0

/**
 * Initialize default settings for the module
 */
void issueInitCommands() {
	while (!sim800l->disableEcho()) {
		#if (defined DEBUG)
		debugStream.println(F("Module not ready, waiting"));
		#endif
		delay(500);
	}
	sim800l->enableCallerIdentification();
	#if (defined DEBUG)
	debugStream.println(F("Init commands done"));
	#endif
}

void gsmSetup() {
	// Initialize Serial Monitor for debugging
	// TODO: extract from here
	#if (defined DEBUG)
	debugStream.begin(57600);
	while(!debugStream);
	debugStream.println(F("HardwareSerial ready"));
	#endif

	// Initialize a SoftwareSerial
	SoftwareSerial* serial = new SoftwareSerial(SIM800_RX_PIN, SIM800_TX_PIN);
	serial->begin(57600);
	while(!serial);
	#if (defined DEBUG)
	debugStream.println(F("SoftwareSerial ready"));
	#endif
	
	sim800l = new SIM800L((Stream *)serial, RESET_PIN_NOT_USED, 
		#if (defined DEBUG)
		(Stream *)&debugStream
		#else
		(Stream *)NULL
		#endif
	);
	
	issueInitCommands();
	
	#if (defined DEBUG)
	debugStream.println(F("Module ready"));
	#endif
	
	return; // TODO!!!!!!!!!!!!!!!!!!
	/*
	//debugStream.println(F("Start of test protocol"));

	// Wait for the GSM signal
	uint8_t signal = sim800l->getSignal();
	while (signal <= 0) {
		//debugStream.println(F("Waiting for signal"));
		delay(500);
		signal = sim800l->getSignal();
	}

	if(signal > 5) {
		//debugStream.print(F("Signal OK (strength: "));
	} else {
		//debugStream.print(F("Signal low (strength: "));
	}
	//debugStream.print(signal);
	//debugStream.println(F(")"));
	//delay(1000);

	// Wait for operator network registration (national or roaming network)
	NetworkRegistration network = sim800l->getRegistrationStatus();
	while(network != REGISTERED_HOME && network != REGISTERED_ROAMING) {
		//debugStream.println(F("Waiting for registration"));
		delay(500);
		network = sim800l->getRegistrationStatus();
	}
	//debugStream.println(F("Network registration OK"));
	
	sim800l->setupGPRS("internet.mts.ru");
	//debugStream.println(F("GPRS setup done"));
	
	sim800l->disconnectGPRS();
	while (!sim800l->connectGPRS()) {
		//debugStream.println(F("Fail to connect GPRS, retry"));
		delay(500);
	}
	debugStream.println(F("GPRS connection done"));

	uint16_t result = sim800l->doGet("http://fit.ippolitov.me/test.txt", 5000);
	debugStream.print(F("HTTP code: "));
	debugStream.println(result);
	
	char body[RECV_BUFFER_SIZE];
	strncpy(body, sim800l->getDataReceived(), RECV_BUFFER_SIZE);
	uint16_t size = sim800l->getDataSizeReceived();
	body[size] = '\0';
	debugStream.print(F("HTTP body size: "));
	debugStream.println(size);
	debugStream.print(F("HTTP body: "));
	debugStream.println(body);
	
	debugStream.println(F("End of test protocol"));
	*/
}


bool gsmAvailable() {
	return sim800l->hasIncomingData();
}

void gsmProcess() {
	if (sim800l->readIncoming(100)) {
		const char* data = sim800l->getDataReceived();
		#if (defined DEBUG)
		debugStream.print(F("INCOMING DATA: >>"));
		debugStream.print(data);
		debugStream.println(F("<<"));
		#endif
		if (strncmp_P(data, NOTIF_CLIP, sizeof(NOTIF_CLIP)-1) == 0) {
			char phoneNumber[12] = {0};
			strncpy(phoneNumber, &data[sizeof(NOTIF_CLIP)], sizeof(phoneNumber)-1);
			processGsmRing(phoneNumber);
		} else {
			#if (defined DEBUG)
			debugStream.println(F("Unknown stuff, ignoring"));
			#endif
		}
	}
}

void gsmHangupCall() {
	sim800l->hangupCall();
}

