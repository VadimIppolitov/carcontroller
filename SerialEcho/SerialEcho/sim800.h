
#if (!defined SIM800_H)
#define SIM800_H

void gsmSetup();

bool gsmAvailable();

void gsmProcess();

void gsmHangupCall();

#endif