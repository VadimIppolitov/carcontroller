
#include "../car_status.h"

int statusSentCount = 0;

bool wasStatusSent() {
	return statusSentCount > 0;
}

void clearStatusSentCount() {
	statusSentCount = 0;
}

void sendStatus(const CarStatus& carStatus, bool ignitionStatus, unsigned int minutesUntilScheduledStart) {
	statusSentCount++;
}

void sendEvents(unsigned int firstEvent, unsigned int lastEvent) {
}
