#include <string.h>
#include <stdio.h>

#include "testLib.h"
#include "../message.h"

#define MSG "Let's\xF1playТест"

bool glitchHappened = false;
unsigned char limit = 0;
unsigned char pos = 0;
unsigned char buf[256];

int16_t btRead() {
	return pos < limit ? buf[pos++] : '*';
}

void btSend(const uint8_t *data, int16_t len) {
	for (int16_t i = 0; i < len; ++i) {
		buf[limit+i] = data[i];
	}
	limit += len;
}

bool btAvailable() {
	if (!glitchHappened && pos == 7) {
		glitchHappened = true;
		return false;
	}
	return pos < limit;
}

int main(void) {
	int len = strlen(MSG);
	Message msgPrep = {};

	msgPrep.size = len;
	memcpy(&msgPrep.data[0], (uint8_t*)MSG, len);
	btSendMessage(&msgPrep);

	Message msg;
	clearMessage(&msg);
	assertFalse(btReadMessage(&msg), "Message unexpectedly read");
	assertTrue(btReadMessage(&msg), "Message not read");
	printf("size(");
	fwrite(&msg.data[msg.pos], 1, msg.size, stdout);
	printf(") = %d\n", msg.size);
	assertEquals(len, msg.size, "Wrong size");
	assertEquals(0, strncmp((char*)&msg.data[msg.pos], MSG, len), "Data not read correctly");
}
