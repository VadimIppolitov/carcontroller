#include <string.h>
#include <stdio.h>

#include "testLib.h"
#include "../message.h"

#define MSG1 "1Let's\xF1play1Тест1"
#define MSG2 "2Let's\xF1play2Тест22"
#define MSG3 "3Let's\xF1play3Тест333"

uint8_t limit = 0;
uint8_t pos = 0;
uint8_t buf[256]; // Enough space for three messages and preambles

int16_t btRead() {
	return pos < limit ? buf[pos++] : '*';
}

void btSend(const uint8_t *data, int16_t len) {
	for (uint8_t i = 0; i < len; ++i) {
		buf[limit+i] = data[i];
	}
	limit += len;
}

bool btAvailable() {
	if (pos == 7) {
		pos = 13;
		return false;
	}
	return pos < limit;
}

void printMessage(Message *msg) {
	printf("size=%d, pos=%d, limit=%d, data=", msg->size, msg->pos, msg->limit);
	fwrite(&msg->data[msg->pos], 1, msg->size, stdout);
	printf("\n");
}

void send(const char *text) {
	int len = strlen(text);
	Message msgPrep = {};

	msgPrep.size = len;
	memcpy(&msgPrep.data[0], text, len);
	btSendMessage(&msgPrep);
}

int main(void) {
	send(MSG1);
	send(MSG2);
	send(MSG3);

	Message msg;
	bool status;

	clearMessage(&msg);
	assertFalse(btReadMessage(&msg), "Message unexpectedly read");
	status = btReadMessage(&msg);
	printMessage(&msg);
	assertTrue(status, "Message not read");
	printf("size(");
	fwrite(&msg.data[msg.pos], 1, msg.size, stdout);
	printf(") = %d\n", msg.size);
	int len = strlen(MSG1);
	assertEquals(len, msg.size, "Wrong size");
	assertNotEquals(0, strncmp((char*)&msg.data[msg.pos], MSG1, len), "Data unexpectedly read correctly");

	clearMessage(&msg);
	status = btReadMessage(&msg);
	printMessage(&msg);
	assertTrue(status, "Message not read");
	printf("size(");
	fwrite(&msg.data[msg.pos], 1, msg.size, stdout);
	printf(") = %d\n", msg.size);
	len = strlen(MSG3);
	assertEquals(len, msg.size, "Wrong size");
	assertEquals(0, strncmp((char*)&msg.data[msg.pos], MSG3, len), "Data not read correctly");
}
