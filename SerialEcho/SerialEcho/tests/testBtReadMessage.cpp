#include <string.h>
#include <stdio.h>

#include "testLib.h"
#include "../message.h"

#define MSG_LEN 18
#define MSG "Let's\xF1playТест"

unsigned int ptr = 0;
unsigned char data[] = MSG "THIS_SHOULD_NOT_BE_READ";
bool needPreamble = true;
bool needLen = true;

unsigned char rawData[PREAMBLE_LENGTH + 1 + MSG_LEN] = {0};

uint16_t btRead() {
	return ptr < sizeof(rawData) ? rawData[ptr++] : '*';
}

bool btAvailable() {
	return ptr < sizeof(rawData);
}

int main(void) {
	assertEquals(MSG_LEN, strlen(MSG), "Bug in test, string length doesn't match");

	//  prepare
	rawData[PREAMBLE_LENGTH] = MSG_LEN;
	strncpy((char *)&rawData[1 + PREAMBLE_LENGTH], (char *)data, MSG_LEN);

	Message msg;
	clearMessage(&msg);
	assertTrue(btReadMessage(&msg), "Message not read");
	printf("size(");
	fwrite(&msg.data[msg.pos], 1, msg.size, stdout);
	printf(") = %d\n", msg.size);
	assertEquals(0, strncmp((char*)&msg.data[msg.pos], (char*)&data[0], MSG_LEN), "Data not read correctly");
	assertEquals(MSG_LEN, msg.size, "Wrong size");
}
