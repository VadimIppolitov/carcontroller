#include <string.h>
#include <stdio.h>

#include "testLib.h"
#include "../message.h"

#define STR "ABCDEFGHI"

bool preambleReceived;
bool sizeReceived;
bool contentReceived;

const char PREAMBLE[PREAMBLE_LENGTH] = {0};

void btSend(const uint8_t *data, int16_t len) {
	if (!preambleReceived) {
		assertEquals(PREAMBLE_LENGTH, len, "Bad size of preamble");
		assertEquals(0, strncmp(PREAMBLE, (const char *)data, PREAMBLE_LENGTH), "Bad preamble content");
		preambleReceived = true;
	} else if (!sizeReceived) {
		assertEquals(1, len, "Bad size of message size");
		assertEquals(strlen(STR), data[0], "Bad message size when sending size");
		sizeReceived = true;
	} else if (!contentReceived) {
		assertEquals(strlen(STR), len, "Bad message size when sending message");
		assertEquals(0, strncmp((const char *)data, STR, len), "Bad message content");
		contentReceived = true;
	} else {
		fail("Extra message received");
	}
}

void fillMessage(Message *msg) {
	msg->size = strlen(STR);
	msg->pos = 0;
	msg->limit = msg->size;
	
	strcpy((char*)&msg->data[0], STR);
}

void reset() {
	preambleReceived = sizeReceived = contentReceived = false;
}

void testBtSendMessage(Message *msg) {
	reset();

	fillMessage(msg);
	btSendMessage(msg);

	assertTrue(sizeReceived, "Size not received");
	assertTrue(contentReceived, "Content not received");
}

int main(void) {
	Message msg;
	printf("testBtSendMessage(run 1)\n");
	testBtSendMessage(&msg);
	clearMessage(&msg);
	printf("testBtSendMessage(run 2)\n");
	testBtSendMessage(&msg);
}
