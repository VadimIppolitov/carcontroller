#include <string.h>
#include <stdio.h>

#include "testLib.h"
#include "../message.h"
#include "../crypt.h"

int main(void) {
	Message msg;
	CarMessage carMsg = {
		.type = MSG_STATUS,
	};

	carMsg.msgStatus.ignitionStatus = true;
	carMsg.msgStatus.generatorStatus = true;
	carMsg.msgStatus.gearStatus = 'P';
	carMsg.msgStatus.temperature = 20;
	carMsg.msgStatus.lastStatus = 1;
	carMsg.msgStatus.lastStarterTime = 12345;
	carMsg.msgStatus.accStatus = false;
	carMsg.msgStatus.lastEventSeq = 22334;

	printData("carMsg", &carMsg, sizeof(carMsg));

	encrypt(carMsg, &msg);
	
	assertNotEquals(0, memcmp(&msg.data[msg.pos], &carMsg, sizeof(carMsg)), "Message seems not encrypted");

	printData("ciphertext", &msg.data[msg.pos], msg.size);

	CarMessage decrypted;
	memset(&decrypted, 0, sizeof(CarMessage));

	assertTrue(decrypt(&msg, &decrypted), "Message failed to decrypt");

	printData("decrypted", &decrypted, sizeof(decrypted));

	assertEquals(0, memcmp(&decrypted, &carMsg, sizeof(carMsg)), "Message not decrypted correctly");
}
