#include <string.h>
#include <stdio.h>
#include <inttypes.h>

#include "testLib.h"

#include "../defer.h"

#define SLOT 0

uint32_t faketime = 123456;
bool flag;
bool rescheduleFlag;

uint32_t currentMs() {
	return faketime;
}

void callbackOnce() {
	flag = true;
}

void callbackReschedule() {
	rescheduleFlag = true;
	defer(SLOT, &callbackOnce, 200);
}

void testDeferOnce() {
	flag = false;
	defer(SLOT, &callbackOnce, 300);
	assertFalse(flag, "Callback called ahead of time");
	runDeferred();
	assertFalse(flag, "Callback called ahead of time");
	faketime += 299;
	runDeferred();
	assertFalse(flag, "Callback called ahead of time");
	faketime += 1;
	runDeferred();
	assertTrue(flag, "Callback not called");
	assertFalse(isActiveDefer(SLOT), "Callback not cleared");
}

void testDeferReschedule() {
	flag = false;
	rescheduleFlag = false;
	defer(SLOT, &callbackReschedule, 100);
	runDeferred();
	assertFalse(rescheduleFlag, "CallbackReschedule called ahead of time");
	assertFalse(flag, "Callback called ahead of time");
	faketime += 100;
	runDeferred();
	assertTrue(rescheduleFlag, "CallbackReschedule not called");
	assertFalse(flag, "Callback called ahead of time");
	assertTrue(isActiveDefer(SLOT), "Callback unexpectedly cleared");
	faketime += 200;
	runDeferred();
	assertTrue(flag, "Callback not called");
	assertFalse(isActiveDefer(SLOT), "Callback not cleared");
}

void testDeferOverflow() {
	flag = false;
	faketime = (1ULL << 32) - 100;
	defer(SLOT, &callbackOnce, 200);
	printf("fakeTime(1) = %" PRIu32 ", timeRemaining = %" PRIi32 "\n", faketime, timeRemaining(SLOT));
	assertFalse(flag, "Callback called ahead of time (1.1)");
	runDeferred();
	assertFalse(flag, "Callback called ahead of time (1.2)");
	faketime += 99;
	printf("fakeTime(2) = %" PRIu32 ", timeRemaining = %" PRIi32 "\n", faketime, timeRemaining(SLOT));
	runDeferred();
	assertFalse(flag, "Callback called ahead of time (2)");
	faketime += 100;
	printf("fakeTime(3) = %" PRIu32 ", timeRemaining = %" PRIi32 "\n", faketime, timeRemaining(SLOT));
	runDeferred();
	assertFalse(flag, "Callback called ahead of time (3)");
	faketime += 1;
	printf("fakeTime(4) = %" PRIu32 ", timeRemaining = %" PRIi32 "\n", faketime, timeRemaining(SLOT));
	runDeferred();
	assertTrue(flag, "Callback not called");
	assertFalse(isActiveDefer(SLOT), "Callback not cleared");
}

int main(void) {
	printf("testDeferOnce()\n");
	testDeferOnce();

	printf("testDeferReschedule()\n");
	testDeferReschedule();

	printf("testDeferOverflow()\n");
	testDeferOverflow();
}
