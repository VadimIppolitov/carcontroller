#include <string.h>
#include <stdio.h>

#include "testLib.h"

#include "../event_storage.h"

//unsigned long currentMs() {
//	return faketime;
//}

unsigned long secondsSinceEpoch() {
	return 0;
}

StoredEvent input;
StoredEvent output;

void assertInputOutputEqual() {
	assertEquals(input.event, output.event, "'event' differs");
	assertEquals(input.initiatedBy, output.initiatedBy, "'initiatedBy' differs");
	assertEquals(input.success, output.success, "'success' differs");
}

void prepareEventNumber(unsigned char n) {
	//  Use fields to tell the difference between events
	input.event = n; 
	input.initiatedBy = n;
	input.success = n % 2 == 0;
}

void testStoreFirst() {
	clearAllEvents();
	prepareEventNumber(0);
	assertEquals(0, storeEvent(&input), "Event stored as non-zero");
	assertTrue(fetchEvent(0, &output), "Event not fetched");
	assertInputOutputEqual();
	assertFalse(fetchEvent(1, &output), "Non-existing event fetched");
}

void testStoreMax() {
	clearAllEvents();
	for (int i = 0; i < MAX_STORED_EVENTS; ++i) {
		prepareEventNumber(i);
		assertEquals(i, storeEvent(&input), "Event stored as wrong seq");
	}
	for (int i = 0; i < MAX_STORED_EVENTS; ++i) {
		//printf("Checking %d\n", i);
		prepareEventNumber(i);
		assertTrue(fetchEvent(i, &output), "Event not fetched");
		assertInputOutputEqual();
	}
}

void testOverflow() {
	clearAllEvents();
	int howMany = (int)(MAX_STORED_EVENTS * 2.7);
	printf("Storing %d events\n", howMany);
	for (int cur = 0; cur < howMany; ++cur) {
		//printf("Loop index: %d\n", cur);
		prepareEventNumber(cur);
		assertEquals(cur, storeEvent(&input), "Event stored as wrong seq");

		//  Calculate seq of the oldest event which is still alive
		int oldestOk = 0;
		if (cur >= MAX_STORED_EVENTS) {
			oldestOk = cur - MAX_STORED_EVENTS + 1;
		}

		//  Check that all events which should be alive are OK
		for (int j = oldestOk; j <= cur; ++j) {
			//printf("Checking %d\n", j);
			prepareEventNumber(j);
			assertTrue(fetchEvent(j, &output), "Event not fetched");
			assertInputOutputEqual();
		}

		//  Check that all events which should be dead are dead
		for (int j = 0; j < oldestOk; ++j) {
			//printf("Checking %d\n", j);
			bool result = fetchEvent(j, &output);
			assertFalse(result, "Fetched event which was overwritten");
			//prepareEventNumber(j);
			//assertInputOutputEqual();
		}
		assertFalse(fetchEvent(cur+1, &output), "Non-existing event fetched");
		//printf("\n");
	}
}

int main(void) {
	printf("testStoreFirst()\n");
	testStoreFirst();

	printf("testStoreMax()\n");
	testStoreMax();

	printf("testOverflow()\n");
	testOverflow();
}
