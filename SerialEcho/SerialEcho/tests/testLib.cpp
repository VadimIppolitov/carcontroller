#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "testLib.h"

void printMsg(const char *msg) {
	printf("%s\n", msg);
}

void failImpl(const char *msg, const char *file, const int line) {
	printf("%s (%s:%d)\n", msg, file, line);
	exit(1);
}

void assertTrueImpl(int value, const char *msg, const char *file, const int line) {
	if (value == 0) {
		printf("%s: expected nonzero (%s:%d)\n", msg, file, line);
		exit(1);
	}
}

void assertFalseImpl(int value, const char *msg, const char *file, const int line) {
	if (value != 0) {
		printf("%s: expected zero, actual %d (%s:%d)\n", msg, value, file, line);
		exit(1);
	}
}

void assertEqualsImpl(int expected, int actual, const char *msg, const char *file, const int line) {
	if (expected != actual) {
		printf("%s: expected %d, actual %d (%s:%d)\n", msg, expected, actual, file, line);
		exit(1);
	}
}

void assertNotEqualsImpl(int notExpected, int actual, const char *msg, const char *file, const int line) {
	if (notExpected == actual) {
		printf("%s: unexpected value %d (%s:%d)\n", msg, actual, file, line);
		exit(1);
	}
}

unsigned long getRealCurrentMs() {
	struct timeval te;
	gettimeofday(&te, NULL);
	return te.tv_sec*1000LL + te.tv_usec/1000;
}


void printData(const char *title, const void *data, const int size) {
	printf("%s[", title);
	for (int i = 0; i < size; ++i) {
		if (i > 0) {
			printf(", ");
		}
		printf("%.2x", ((unsigned char *)data)[i]);
	}
	printf("]\n");
}
