
//  Actual usable routines

#define fail(msg) failImpl((msg), __FILE__, __LINE__)

#define assertTrue(value, msg) assertTrueImpl((value), (msg), __FILE__, __LINE__)

#define assertFalse(value, msg) assertFalseImpl((value), (msg), __FILE__, __LINE__)

#define assertEquals(expected, actual, msg) assertEqualsImpl((expected), (actual), (msg), __FILE__, __LINE__)

#define assertNotEquals(notExpected, actual, msg) assertNotEqualsImpl((notExpected), (actual), (msg), __FILE__, __LINE__)

unsigned long getRealCurrentMs();

//  Implementations, not to be used

void failImpl(const char *msg, const char *file, const int line);
void assertTrueImpl(int value, const char *msg, const char *file, const int line);
void assertFalseImpl(int value, const char *msg, const char *file, const int line);
void assertEqualsImpl(int expected, int actual, const char *msg, const char *file, const int line);
void assertNotEqualsImpl(int notExpected, int actual, const char *msg, const char *file, const int line);

void printData(const char *title, const void *data, const int size);
