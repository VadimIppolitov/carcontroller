#include <string.h>
#include <stdio.h>

#include "testLib.h"
#include "../car.h"
#include "../car_reports.h"
#include "../car_status.h"
#include "../message.h"
#include "../defer.h"
#include "../slots.h"
#include "../event.h"
#include "../event_storage.h"
#include "../engine.h"
#include "../engine_start_status.h"

#include "dummyForStatus.h"

#define DONT_TURN_ON (-1)

bool ignited = false;
bool led = true;
bool acc = false;
bool genOn = false;
bool parked = false;
bool starterTurnedOn = false;
bool starterTurnedOff = false;
int checksLeftBeforeTurningOnGen = DONT_TURN_ON;
uint32_t fakeTime = 0;

extern CarStatus carStatus;

bool isGeneratorOn() {
	if (!genOn && checksLeftBeforeTurningOnGen != DONT_TURN_ON && --checksLeftBeforeTurningOnGen <= 0) {
		printf("Turning on generator\n");
		genOn = true;
	}
	return genOn;
}

void sendText(char const *text) {
}

void ignitionOff() {
	ignited = false;
}

void ignitionOn() {
	ignited = true;
}

void statusLedOff() {
	led = false;
}

void statusLedOn() {
	led = true;
}

void accOff() {
	acc = false;
}

void accOn() {
	acc = true;
}

bool isAccOn() {
	return acc;
}

void delayMs(uint16_t ms) {
}

bool isParked() {
	return parked;
}

void starterOn() {
	starterTurnedOn = true;
}

void starterOff() {
	starterTurnedOff = true;
}

void sendSettings(const Settings& settings) {
}

uint32_t skipTime(int slot) {
	uint32_t skippedTime = timeRemaining(slot);
	fakeTime += skippedTime;
//	printf("fakeTime=%ld, (skipped=%ld)\n", fakeTime, skippedTime);
	return skippedTime;
}

uint32_t skipTime() {
	return skipTime(SL_ENGINE_START);
}

void dumpEvents() {
	for (unsigned int i = 0; i <= getLastSeq(); ++i) {
		StoredEvent event;
		fetchEvent(i, &event);
		printf("event %d: type=%d, by=%d, success=%d\n", i, event.event, event.initiatedBy, event.success);
	}
}

void checkStoredEvent(uint8_t event, uint8_t by, bool expectedActionSuccess) {
//	dumpEvents();
	StoredEvent e;
	assertEquals(0, getLastSeq(), "Wrong lastSeq");
	fetchEvent(0, &e);
	assertEquals(event, e.event, "Wrong event");
	assertEquals(by, e.initiatedBy, "Wrong initiatedBy");
	assertEquals(expectedActionSuccess, e.success, "Wrong action success");
	clearAllEvents();
}

void checkStoredEvent(bool expectedActionSuccess) {
	checkStoredEvent(EVENT_ENGINE_START, EV_BY_USER, expectedActionSuccess);
}

uint32_t currentMs() {
	return fakeTime;
}

uint32_t secondsSinceEpoch() {
	return fakeTime / 1000;
}

//  tests

void clearStatus() {
	carStatus.lastStatus = 0;
	carStatus.accStatus = false;
	carStatus.parked = false;
	carStatus.generator = false;
	initDefaultSettings(&carStatus.settings);
}

void clearVars() {
	genOn = false;
	parked = false;
	ignited = false;
	led = false;
	acc = false;
	statusSentCount = 0;
	starterTurnedOn = false;
}

void startEngine() {
	startEngine(10);
}

void iteration() {
	carBackgroundActions();
}

void assertStatusSent() {
	assertTrue(wasStatusSent(), "Wrong status sent");
}

void testStartEngineWhenNotParkedAndGenOff() {
	clearStatus();
	clearVars();

	startEngine();
	assertEquals(STARTING, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(isActiveDefer(SL_ENGINE_START), "Callback not installed");
	assertEquals(3000, skipTime(), "Init time wrong");
	iteration();

	assertEquals(NOT_PARKED, carStatus.lastStatus, "Wrong lastStatus");
	assertFalse(ignited, "Ignition is on");
	assertFalse(led, "Led is on");
	assertEquals(false, genOn, "Generator is in wrong state");
	assertFalse(starterTurnedOn, "Starter turned on");
	assertStatusSent();
	assertFalse(isActiveDefer(SL_ENGINE_START), "Callback not cleared");
	checkStoredEvent(false);
}

void testStartEngineWhenNotParkedAndGenOn() {
	clearStatus();
	clearVars();
	genOn = true;

	startEngine();
	runDeferred();

	assertEquals(ALREADY_STARTED, carStatus.lastStatus, "Wrong lastStatus");
	assertFalse(ignited, "Ignition is on");
	assertFalse(led, "Led is on");
	assertEquals(true, genOn, "Generator is in wrong state");
	assertFalse(starterTurnedOn, "Starter turned on");
	assertStatusSent();
	assertFalse(isActiveDefer(SL_ENGINE_START), "Callback not cleared");
	checkStoredEvent(false);
}

void testStartEngineWhenParkedAndGenOn() {
	clearStatus();
	clearVars();
	parked = true;
	genOn = true;

	startEngine();
	runDeferred();

	assertEquals(HOLD, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(ignited, "Ignition is off");
	assertTrue(led, "Led is off");
	assertTrue(genOn, "Generator is off");
	assertFalse(starterTurnedOn, "Starter turned on");
	assertStatusSent();
	assertFalse(isActiveDefer(SL_ENGINE_START), "Callback not cleared");
	checkStoredEvent(true);
}

void testStartEngineWhenParkedAndGenOffNoGen() {
	clearStatus();
	clearVars();
	parked = true;

	startEngine();
	assertEquals(STARTING, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(isActiveDefer(SL_ENGINE_START), "Callback not installed");
	skipTime();
	iteration();

	assertEquals(ENGINE_TIMEOUT, carStatus.lastStatus, "Wrong lastStatus");
	assertFalse(ignited, "Ignition is on");
	assertFalse(led, "Led is on");
	assertFalse(genOn, "Generator is on");
	assertTrue(starterTurnedOn, "Starter not turned on");
	assertTrue(starterTurnedOff, "Starter not turned off");
	assertStatusSent();
	assertFalse(isActiveDefer(SL_ENGINE_START), "Callback not cleared");
	checkStoredEvent(false);
}

void testStartEngineWhenParkedAndGenOffOK() {
	clearStatus();
	carStatus.accStatus = true;
	clearVars();
	parked = true;
	acc = true;

	startEngine();
	assertEquals(STARTING, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(isActiveDefer(SL_ENGINE_START), "Callback not installed");
	assertFalse(acc, "Acc not turned off");
	skipTime();
	iteration();

	assertEquals(VERIFYING, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(isActiveDefer(SL_ENGINE_START), "Callback not installed");
	assertEquals(1000, skipTime(), "Wrong verification time");
	iteration();

	assertEquals(STARTED, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(ignited, "Ignition is off");
	assertTrue(led, "Led is off");
	assertTrue(genOn, "Generator is off");
	assertTrue(starterTurnedOn, "Starter not turned on");
	assertTrue(starterTurnedOff, "Starter not turned off");
	assertTrue(acc, "Acc not turned back on");
	assertStatusSent();
	assertFalse(isActiveDefer(SL_ENGINE_START), "Callback not cleared");
	assertTrue(isActiveDefer(SL_ENGINE_AUTO_STOP), "Autostop not scheduled");
	checkStoredEvent(true);

	clearStatusSentCount();
	skipTime(SL_ENGINE_AUTO_STOP);
	iteration();
	assertFalse(isActiveDefer(SL_ENGINE_AUTO_STOP), "Autostop schedule not removed");
	assertStatusSent();
	checkStoredEvent(EVENT_ENGINE_STOP, EV_BY_SCHEDULE, true);
}

void testScheduledStartEngineOK() {
	clearStatus();
	clearVars();
	parked = true;

	Settings newSettings = carStatus.settings;
	newSettings.version++;
	newSettings.autostartEnabled = true;

	applySettings(&newSettings);

	assertEquals(newSettings.version, carStatus.settings.version, "Wrong settings version");
	assertEquals(newSettings.autostartEnabled, carStatus.settings.autostartEnabled, "Wrong autostartEnabled");
	assertTrue(isActiveDefer(SL_ENGINE_SCHEDULED_START), "Autostart not scheduled");

	iteration();
	assertStatusSent();

	skipTime(SL_ENGINE_SCHEDULED_START);
	iteration();

	assertEquals(STARTING, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(isActiveDefer(SL_ENGINE_START), "Callback not installed");
	skipTime();
	iteration();

	assertEquals(VERIFYING, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(isActiveDefer(SL_ENGINE_START), "Callback not installed");
	skipTime();
	iteration();

	assertEquals(STARTED, carStatus.lastStatus, "Wrong lastStatus");
	assertTrue(ignited, "Ignition is off");
	assertTrue(led, "Led is off");
	assertTrue(genOn, "Generator is off");
	assertTrue(starterTurnedOn, "Starter not turned on");
	assertTrue(starterTurnedOff, "Starter not turned off");
	assertStatusSent();
	assertFalse(isActiveDefer(SL_ENGINE_START), "Callback not cleared");
	assertTrue(isActiveDefer(SL_ENGINE_AUTO_STOP), "Autostop not scheduled");
	checkStoredEvent(EVENT_ENGINE_START, EV_BY_SCHEDULE, true);

	clearStatusSentCount();
	uint32_t scheduledRunTime = skipTime(SL_ENGINE_AUTO_STOP);
	assertEquals(newSettings.autostartDurationMinutes * 60 * 1000UL, scheduledRunTime + 3000 + 1000, "Scheduled runtime differs");
	iteration();
	assertFalse(isActiveDefer(SL_ENGINE_AUTO_STOP), "Autostop schedule not removed");
	assertStatusSent();
	checkStoredEvent(EVENT_ENGINE_STOP, EV_BY_SCHEDULE, true);
}

//  main

int main(void) {
	printf("testStartEngineWhenNotParkedAndGenOn()\n");
	testStartEngineWhenNotParkedAndGenOn();

	printf("testStartEngineWhenNotParkedAndGenOff()\n");
	testStartEngineWhenNotParkedAndGenOff();

	printf("testStartEngineWhenParkedAndGenOn()\n");
	testStartEngineWhenParkedAndGenOn();

	checksLeftBeforeTurningOnGen = 1000000;  //  Should not happen

	printf("testStartEngineWhenParkedAndGenOffNoGen()\n");
	testStartEngineWhenParkedAndGenOffNoGen();

	checksLeftBeforeTurningOnGen = 7;

	printf("testStartEngineWhenParkedAndGenOffOK()\n");
	testStartEngineWhenParkedAndGenOffOK();

	checksLeftBeforeTurningOnGen = 7;

	printf("testScheduledStartEngineOK()\n");
	testScheduledStartEngineOK();

}
