#include <string.h>
#include <stdio.h>

#include "testLib.h"
#include "../car.h"
#include "../message.h"
#include "../crypt.h"
#include "../defer.h"
#include "../slots.h"
#include "../engine.h"
#include "../engine_start_status.h"

#include "dummyForStatus.h"

bool ignited = true;
bool led = true;
bool btSent = false;

void ignitionOff() {
	ignited = false;
}

void statusLedOff() {
	led = false;
}

/*
void btSendMessage(const Message *msg) {
	CarMessage carMsg;
	unpackMsg(msg, &carMsg);
	printf("carMsg.type=%d\n", carMsg.type);
	if (carMsg.type == MSG_STATUS) {
		btSent = true;
	}
}
*/

void dummyAutoStop() {
}

unsigned long currentMs() {
	return 0;
}

extern CarStatus carStatus;

int main(void) {
	carStatus.lastStatus = STARTED;
	defer(SL_ENGINE_AUTO_STOP, &dummyAutoStop, 1);
	
	turnIgnitionOff();
	runDeferred();
	
	assertEquals(0, carStatus.lastStatus, "Wrong status");
	assertFalse(ignited, "Wrong ignited");
	assertFalse(led, "Wrong led");
}
