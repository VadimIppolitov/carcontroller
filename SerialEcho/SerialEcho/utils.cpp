#include <Arduino.h>
#include <stdlib.h>

#include "DS3232RTC.h"
#include "io.h"

void delayMs(uint16_t milliseconds) {
	delay(milliseconds);
}

void blink(uint16_t ms) {
	statusLedOn();
	delay(ms);
	statusLedOff();
	delay(ms);
}

uint32_t currentMs() {
	return millis();
}

uint32_t currentMicros() {
	return micros();
}

uint32_t secondsSinceEpoch() {
	return RTC.get();
}

float temperatureCelsius() {
	return RTC.temperature() / 4.0;
}

void generateRandomSeed() {
	uint32_t result = 0;
	for (int16_t i = 0; i < 31; ++i) {
		int16_t pinVal = readFloatingPin();
		result |= (uint32_t)(pinVal & 1) << i;
	}
	srandom(result);
}

uint16_t randomUInt() {
	return random() & 0xFFFF;
}