
#if (!defined UTILS_H)
#define UTILS_H

#include <stdint.h>

#define min(a,b) ((a)<(b)?(a):(b))

void delayMs(uint16_t milliseconds);

void blink(uint16_t ms);

uint32_t currentMs();

uint32_t currentMicros();

uint32_t secondsSinceEpoch();

float temperatureCelsius();

void generateRandomSeed();

uint16_t randomUInt();

#endif