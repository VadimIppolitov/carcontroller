#include <stdio.h>
#include <string.h>
#include <sodium.h>
#include <fcntl.h>

#define CIPHERTEXT_LEN (crypto_secretbox_MACBYTES + MESSAGE_LEN)

int main() {
  char message[12];
  unsigned char nonce[crypto_secretbox_NONCEBYTES] = {0};
  unsigned char key[crypto_secretbox_KEYBYTES];
  unsigned char cyphertext[crypto_secretbox_MACBYTES + sizeof(message)];
  unsigned char hex[1024];

  int key_file = open("key.bin", O_RDONLY);
  if (key_file == -1) {
    printf("Failed to open key file for reading\n");
    return 1;
  }
  if (read(key_file, key, sizeof(key)) != sizeof(key)) {
    printf("Failed to write to file\n");
    return 1;
  }
  if (close(key_file) == -1) {
    printf("Failed to close file\n");
    return 1;
  }

  if (sodium_init() == -1) {
    printf("Failed to init libsodium\n");
    return 1;
  }

  while (1) {
    if (fgets(message, sizeof(message), stdin) == NULL) {
      printf("No line\n");
      return 0;
    }
    size_t message_len = strlen(message);
    memset(&message[message_len], 0, sizeof(message) - message_len);
    sodium_bin2hex(hex, sizeof(hex), message, sizeof(message));
    printf("message   = %s\n", hex);

    ++*((uint32_t*)nonce);

    crypto_secretbox_easy(cyphertext, message, sizeof(message), nonce, key);

    sodium_bin2hex(hex, sizeof(hex), nonce, sizeof(nonce));
    printf("nonce     = %s\n", hex);
    sodium_bin2hex(hex, sizeof(hex), cyphertext, sizeof(cyphertext));
    printf("cypher    = %s\n", hex);

    char decrypted[sizeof(message)+1];
    if (crypto_secretbox_open_easy(decrypted, cyphertext, sizeof(cyphertext), nonce, key) == 0) {
      sodium_bin2hex(hex, sizeof(hex), decrypted, sizeof(message));
      printf("decrypted = %s\n", hex);
      decrypted[sizeof(decrypted)-1] = 0;
      printf("%s\n", decrypted);
    } else {
      printf("Error\n");
    }
  }
}
