#include <sodium.h>
#include <fcntl.h>

int main() {
  unsigned char key[crypto_secretbox_KEYBYTES];
  unsigned char hex[1024];


  int key_file = open("key.bin", O_CREAT | O_WRONLY);
  if (key_file == -1) {
    printf("Failed to open key file for writing\n");
    return key_file;
  }

  if (sodium_init() == -1) {
    printf("Failed to init libsodium\n");
    return 1;
  }

  randombytes_buf(key, sizeof(key));
  if (write(key_file, key, sizeof(key)) < sizeof(key)) {
    printf("Failed to write to file\n");
    return 1;
  }

  if (close(key_file) == -1) {
    printf("Failed to close file\n");
    return 1;
  }


  sodium_bin2hex(hex, sizeof(hex), key, sizeof(key));
  printf("OK, key = %s\n", hex);
  return 0;
}
